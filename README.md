## Usage

The tag format is `${OCAML_VERSION}-${RUST_VERSION}`
```
docker pull registry.gitlab.com/dannywillems/docker-ocaml-rust:4.09-1.44.0
```

In a GitLab CI job:
```
build-ocaml-4.09-rust-1.44.0:
  image: registry.gitlab.com/dannywillems/docker-ocaml-rust:4.09-1.44.0
  stage: test
  before_script:
    # !!REQUIRED!! Otherwise, the environment will not use the correct version
    - . $HOME/.cargo/env
  script:
    - ocaml --version
    - rustc --version
```
