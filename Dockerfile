ARG OCAML_VERSION

FROM ocaml/opam:debian-ocaml-${OCAML_VERSION}

ARG RUST_VERSION

RUN sudo apt-get update
RUN sudo apt-get install m4 -y
RUN eval $(opam env)
RUN opam repository set-url default https://opam.ocaml.org
RUN opam update
RUN opam depext -i -y conf-rust
# Install rustup to manage different Rust versions. -y is required to bypass
# conf-rust installing cargo globally
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="${HOME}/.cargo/env:${PATH}"
# Install the rust version we want to deal with
RUN echo "Installing rust version ${RUST_VERSION}"
RUN ${HOME}/.cargo/bin/rustup toolchain install ${RUST_VERSION}
RUN ${HOME}/.cargo/bin/rustup default ${RUST_VERSION}

